import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import io.thp.pyotherside 1.2
import QtMultimedia 5.0
import QtSensors 5.0


MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'camcontrol.localtoast'
    automaticOrientation: false
    width: units.gu(45)
    height: units.gu(75)

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('./'));

            importModule('tremor_sense', function() {
                console.log('tremor_sense module imported');
                python.call('tremor_sense.write_config', [potato.reading.x, potato.reading.y, potato.reading.z], function(returnValue) {
                    console.log('tremor_sense.speak returned ' + returnValue);
                })
                python.call('tremor_sense.close_file', [], function(returnValue) {
                    console.log('tremor_sense.speak returned ' + returnValue);
                })

            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }


    Rectangle {
        id: top
        anchors.fill: parent
        color: "green"
        focus: visible
        MouseArea {
            anchors.fill: parent
            drag.target: top
            drag.axis: Drag.XAndYAxis
            drag.minimumX: 0
            drag.maximumX: root.width
            drag.minimumY: 0
            drag.maximumY: root.height
            acceptedButtons: Qt.AllButtons
            onPressed: top.color = 'blue'
            onReleased: top.color = 'green'
            
        }
        Accelerometer {
            id: potato
            dataRate: 100
            active: false
            onReadingChanged: {
                python.write_config(potato.reading.x, potato.reading.y, potato.reading.z, file)
            }

        }

        Rectangle {
            id: bottom
            width: units.gu(50)
            height: units.gu(35)
            focus: visible
            color: "blue"
            TextArea {
                id: activate
                anchors.fill: parent
                focus: visible
                text: "Nonactive"
            }
                MouseArea {
                    anchors.fill: parent
                    drag.target: bottom
                    drag.axis: Drag.XAndYAxis
                    drag.minimumX: 0
                    drag.maximumX: root.width
                    drag.minimumY: 0
                    drag.maximumY: root.height
                    acceptedButtons: Qt.AllButtons
                    onPressed: {
                        bottom.color = 'blue'
                        potato.active = true
                        activate.text = "ACTIVE" + potato.reading.x
                    }
                    onReleased: {
                        activate.text = "DEACTIVATED"
                        bottom.color = 'yellow'
                        potato.active = false
                        bottom.x = 0
                        bottom.y = 0
                        python.close_file()
                    }
                }
            }
        }

}
